# Fourier Transform Intuition

Python Re-Implementation of the excellent
[But what is the Fourier Transform? A visual introduction.](https://www.youtube.com/watch?v=spUNpyF58BY)
intuition video by [3Blue1Brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw).

<p align="center">
  <img src="images/fourier.gif" alt="Fourier Transform">
</p>
