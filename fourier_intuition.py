#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.animation as animation


def to_vector(z):
    return np.array([z.real, z.imag])


def main(to_save=False):
    T = 2
    Nt = 4 * T
    St = 100
    t = np.linspace(0, Nt, St * T + 1)[:, np.newaxis]
    x = np.cos(2 * np.pi * t / T) + 1

    Nf = 2 / T
    Sf = 250
    f = np.linspace(0, Nf, int(Sf * Nf) + 1)
    M = np.exp(-2 * np.pi * 1j * f * t) * x
    X = np.mean(M, axis=0)

    fig, ax_arr = plt.subplots(2)
    fig.tight_layout()
    ax_arr[0].plot(t, x)
    ax_arr[0].set_title(r'$x\left(t\right) = cos\left(\frac{2\pi t}{T} + 1\right)$')
    ax_arr[0].grid()
    ax_arr[1].plot(f, X.real, label='Real')
    ax_arr[1].plot(f, X.imag, label='Imaginary')
    ax_arr[1].set_xlim([0, Nf])
    ax_arr[1].set_ylim([-1, 1])
    ax_arr[1].set_title(r'$X\left(f\right) = \mathcal{FT}\left\{x(t)\right\}$')
    ax_arr[1].legend()

    Xf = np.fft.fft(x[:, 0])
    fig, ax = plt.subplots()
    ax.plot(np.fft.fftfreq(len(t)), Xf.real, label='Real')
    ax.plot(np.fft.fftfreq(len(t)), Xf.imag, label='Imaginary')
    ax.legend()

    fig = plt.figure()
    fig.tight_layout()
    ax0 = fig.add_subplot(211)
    ax1 = fig.add_subplot(223)
    ax2 = fig.add_subplot(224, aspect='equal')

    ax0.plot(t, x)
    ax0.set_title(r'$x\left(t\right) = cos\left(\frac{2\pi t}{T} + 1\right)$')
    ax0.grid()

    artists = []
    artists.append(ax1.plot([], [], 'b', label='Real')[0])
    artists.append(ax1.plot([], [], 'r', label='Imaginary')[0])
    ax1.legend()
    ax1.set_xlim([0, Nf])
    ax1.set_ylim([-1, 1])

    artists.append(ax2.plot([], [])[0])
    artists.append(ax2.plot([], [], 'r.')[0])
    ax2.plot()
    ax2.set_xlim([-2.5, 2.5])
    ax2.set_ylim([-2.5, 2.5])
    ax2.minorticks_on()
    ax2.grid(which='major', linestyle='-')
    ax2.grid(which='minor', linestyle='--', alpha=0.5)
    unit_circle = plt.Circle((0, 0), 1.0, color='k', fill=False)
    ax2.add_artist(unit_circle)

    def animate(fi):
        artists[0].set_data(np.array([f[:fi], X[:fi].real]))
        artists[1].set_data(np.array([f[:fi], X[:fi].imag]))
        artists[2].set_data(to_vector(M[:, fi]))
        artists[3].set_data(to_vector(X[fi]))
        return artists

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # for k in range(0, M.shape[1], 50):
        # a = M[:, k]
        # ax.plot(a.real, a.imag, k)

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_surface(t, f, M.real, rstride=1, cstride=1, cmap=cm.jet)
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_surface(t, f, M.imag, rstride=1, cstride=1, cmap=cm.jet)

    fourier_animation = animation.FuncAnimation(fig, animate, range(f.shape[0]),
                                                blit=True, repeat=False)
    if to_save:
        fourier_animation.save('fourier.gif', dpi=70, writer='imagemagick')
    else:
        plt.show()

if __name__ == "__main__":
    to_save = False
    if len(sys.argv) > 1:
        to_save = sys.argv[1] == 'save'
    main(to_save)
